package com.odoo.aspire;


import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;



	public abstract class Operations {
		static WebDriver driver;
		public static void driverInitializations(String browser) {
		
			if(browser.equalsIgnoreCase("Chrome")) {
				System.setProperty("webdriver.chrome.driver","C:\\Users\\Ruben\\workingSpace\\aspire\\Drivers\\chromedriver.exe");
				driver=new ChromeDriver();
				driver.manage().window().maximize();
			}
			
			else if(browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver","C:\\Users\\Ruben\\workingSpace\\aspire\\Drivers\\IEDriverServer.exe");
				DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
				caps.setCapability("ignoreZoomSetting", true);
				driver = new InternetExplorerDriver(caps);
				driver.manage().window().maximize();
		
			}

		}
		
		public static WebDriver driver() { return driver;}
		
		public static void close() {driver.close();}
		
		public static void sendKeys(By by,String value) {	
			driver.findElement(by).clear();driver.findElement(by).sendKeys(Keys.BACK_SPACE);driver.findElement(by).sendKeys(value);}
		
		public static void openURL(String URL) {driver.navigate().to(URL);}
		
		public static void delay() {driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);}
		
		public static String textValue(By by) {return driver.findElement(by).getText();}
		
		public static void click(By by) {driver.findElement(by).click();}
		
		public static void keyFunction(By by) {driver.findElement(by).sendKeys(Keys.ENTER);}
		
		public static void searchWithReferenceNumber(By by) {driver.findElement(by).sendKeys(Keys.ENTER);}
		
		public static void sleep() throws InterruptedException {Thread.sleep(4000);}
		
		public static void screenShot() throws IOException {
			String destination="C:/Users/Ruben/workingSpace/aspire/screenShot/vaildation.png";
			File source=((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		    File dest=new File(destination);
		    FileUtils.copyFile(source,dest);

		}
		}




