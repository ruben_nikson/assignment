package com.odoo.aspire;

import org.openqa.selenium.By;

public class Locators {
	public By userName=By.id("login");
	
	public By password=By.id("password");
	
	public By loginButton=By.xpath("//button[@class='btn btn-primary btn-block']");
	
	public By inventory=By.id("result_app_1");
	
	public By selectProduct=By.xpath("//nav[@class='o_main_navbar']/ul[@class='o_menu_sections']/li[3]");
	
	public By selectProducts=By.xpath("//nav[@class='o_main_navbar']/ul[@class='o_menu_sections']/li[@class='show']/div[@class='dropdown-menu show']/a[1]");
	
	public By createProduct=By.xpath("//button[@class='btn btn-primary o-kanban-button-new']");
	
	public By productName=By.xpath("//input[@name='name']");
	
	public By updateQuantity=By.xpath("//button[@name='action_update_quantity_on_hand']");
	
	public By createQuantity=By.xpath("//button[@class='btn btn-primary o_list_button_add']");
	
	public By quantityLocation=By.xpath("//div[@class='table-responsive']/table[@class='o_list_table table table-sm table-hover table-striped o_list_table_ungrouped']/tbody/tr[1]/td[2]/div/div/input");
	
	public By quantityPackage=By.xpath("//div[@class='table-responsive']/table[@class='o_list_table table table-sm table-hover table-striped o_list_table_ungrouped']/tbody/tr[1]/td[3]/div/div/input");
	
	public By quantitOnHand=By.xpath("//div[@class='table-responsive']/table[@class='o_list_table table table-sm table-hover table-striped o_list_table_ungrouped']/tbody/tr[1]/td[4]/input");
	
	public By saveQuantity=By.xpath("//button[@class='btn btn-primary o_list_button_save']");
    
	public By applicationMenu=By.xpath("//a[@title='Applications']");

	public By manufacturer=By.id("result_app_2");
    
	public By createOrder=By.xpath("//button[@class='btn btn-primary o_list_button_add']");
    
	public By chooseProduct=By.xpath("//div[@name='product_id']/div/input");
    
	public By addNewLine=By.linkText("Add a line");
    
	public By productInComponent=By.xpath("//div[@class='table-responsive']/table[@class='o_list_table table table-sm table-hover table-striped o_list_table_ungrouped']/tbody/tr[1]/td[@class='o_data_cell o_field_cell o_list_many2one o_required_modifier']/div/div/input");
    
	public By consumeCount=By.xpath("//input[@class='o_field_float o_field_number o_field_widget o_input o_required_modifier']");
    
	public By saveOrder=By.xpath("//button[@class='btn btn-primary o_form_button_save']");
    
	public By confirmOrder=By.xpath("//button[@name='action_confirm']");
    
	public By editOrder=By.xpath("//button[@class='btn btn-primary o_form_button_edit']");
    
	public By quantity=By.xpath("//input[@name='qty_producing']");
    
	public By moveToDone=By.xpath("//button[@class='btn btn-primary']");
    
	public By orderReference=By.xpath("//span[@placeholder='Manufacturing Reference']");
    
	public By navigateBackManufacturerDashboard=By.xpath("//li[@class='breadcrumb-item o_back_button']");
    
	public By searchBar=By.xpath("//input[@placeholder='Search...']");
    
	public By orderStatus=By.xpath("//div[@class='table-responsive']/table[@class='o_list_table table table-sm table-hover table-striped o_list_table_ungrouped']/tbody/tr[1]/td[10]/span");

	
}
