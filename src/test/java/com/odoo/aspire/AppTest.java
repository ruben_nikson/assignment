package com.odoo.aspire;


import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class AppTest extends Operations {
	static Locators locator=new Locators();
	
	@Parameters("browser")
	@BeforeClass
	public static void driverInitialization(@Optional("chrome")String browser) {
		driverInitializations(browser);
	}
	
	@Parameters({"userName","password"})
	@Test(priority=1)
	public static void login(@Optional("user@aspireapp.com")String userName,@Optional("@sp1r3app")String password) throws InterruptedException {
		try {
		
		openURL("https://aspireapp.odoo.com");
		sendKeys(locator.userName,userName);
		sendKeys(locator.password,password);
		click(locator.loginButton);
	
		}
catch(Exception e) {
	System.out.println(e);	
}
	}
	
	@Parameters({"productName","location","packages","quantityOnHand"})
	@Test(priority=2)
	public static void productCreation(@Optional("TestingProduct")String productName,@Optional("WH")String location,@Optional("4")String packages,@Optional("10")String quantityOnHand) throws InterruptedException {
		try {
		delay();
		click(locator.inventory);
		click(locator.selectProduct);
		click(locator.selectProducts);
		click(locator.createProduct);
		sendKeys(locator.productName,productName);
		click(locator.updateQuantity);
		click(locator.createQuantity);
		sendKeys(locator.quantityLocation,location);sleep();
		keyFunction(locator.quantityLocation);
		sendKeys(locator.quantityPackage,packages);sleep();
		keyFunction(locator.quantityPackage);
		sendKeys(locator.quantitOnHand,quantityOnHand);sleep();
		click(locator.saveQuantity);
		click(locator.applicationMenu);	
	}
		catch(Exception e) {
			System.out.println(e);	
		}
			}
	@Parameters({"productName","quantityOnHand","orderQuantity"})
	@Test(priority=3)
	public static void orderReferenceCreation(@Optional("TestingProduct")String productName,@Optional("10")String quantityOnHand,@Optional("7")String orderQuantity) throws InterruptedException {
		try {
		delay();
		click(locator.manufacturer);
		click(locator.createOrder);
		sendKeys(locator.chooseProduct,productName);sleep();
		keyFunction(locator.chooseProduct);
		click(locator.addNewLine);
		sendKeys(locator.productInComponent,productName);sleep();
		keyFunction(locator.productInComponent);delay();
		sendKeys(locator.consumeCount,quantityOnHand);sleep();
		keyFunction(locator.consumeCount);sleep();
		click(locator.saveOrder);sleep();
		click(locator.confirmOrder);sleep();
		click(locator.editOrder);delay();
		sendKeys(locator.quantity,orderQuantity);delay();
		click(locator.saveOrder);sleep();
		click(locator.moveToDone);delay();	
		}
	
	catch(Exception e) {
		System.out.println(e);	
	}
		}
		@Test(priority=4)
		public static void statusVaildation() {
			try {
				delay();
		click(locator.navigateBackManufacturerDashboard);
		sendKeys(locator.searchBar,textValue(locator.orderReference));
		searchWithReferenceNumber(locator.searchBar);sleep();
		Assert.assertEquals(textValue(locator.orderStatus),"Done");
		screenShot();
		
	
	}
		catch(Exception e) {
			System.out.println(e);	
		}
			}
	

	@AfterClass
	public static void closeDriver() {
		close();
	}
	
	
	}
	







