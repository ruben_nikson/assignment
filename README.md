**Automation Test script is written to validate login,product creation,manufacturing order item creation and order status**

## Product Creation
1. Navigate to inventory and select Product -> products in top-menu bar.
2. Click on Create button to create a new product.
3. Give a unique name to the product.Update the product name in testng.xml file before executing the script
4. Click on Update Quantity and tr to update the hand on quantity to more than 10. 
5. To complete this, click on create button and provide location and package details along with hand on quantity.
6. Click on Save button to save the product details.

## Manufacturing Order Creation
1. Navigate to mabufacturing module from application menu.
2. Click on Create button to create new manufacturing order.
3. Select the created product.
4. Click on Add a Line hyperink. Select Product name and enter the "To consume" quantity.
5. Save the details.
6. Click on Confirm button to confirm the order. Order Status should chnage to Confirmed.
7. Cick on edit button and update the quantity.
8. Click on save button. Status should change to To Close.
9. Click on Mark as Done, Status should change to Done.

## Validating the Status
1. Navigate back to Manufacturing Order dashboard page and search the created Manufacturing Order.
2. Check the wheather the status is Done and take screenshot for the reference.

All the inputs details like product name,quantity,location,packages,hand on quantity are parameterized for testng.xml. 
Check the inputs before executing the script.
Convert the App class to TestNG and start the execution by **TestNG Suite**
